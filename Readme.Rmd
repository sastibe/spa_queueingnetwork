---
title: "README"
author: "Sebastian Schweer"
date: "September 15, 2018"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Live demontration

Here is a running example to try out:

```{r}


p_12 = 0.1
p_21 = 0.7
n_obs = 1000
burn_in = 5000
lambda_1 = 0.8
lambda_2 = 0.3
G_1 = 0.2
G_2 = 0.5

firstrun <- present_estimates(n_reps = 10, max_lag = 10, p_12 = p_12, p_21 = p_21, n_obs = n_obs, burn_in = burn_in,
                          lambda_1 = lambda_1, lambda_2 = lambda_2, G_1 = G_1, G_2 = G_2,
                          progress = TRUE)

firstrun

ggsave("result_G2_plot.png", plot = firstrun$plot_result_G2, device = "png")
ggsave("delta_result_plot.png", plot = firstrun$plot_delta_result, device = "png")

system(paste0("mpack -s 'Skript durchgelaufen mit ", n_obs, " Beobachtungen: Plot G2' result_G2_plot.png sastibear@gmx.de"))
system(paste0("mpack -s 'Skript durchgelaufen mit ", n_obs, " Beobachtungen: Plot G2' delta_result_plot.png sastibear@gmx.de"))
```

This leads to plots:

```{r, echo=FALSE}
test$plot_result_G2
```


As well as

```{r, echo=FALSE}
test$plot_delta_result
```



